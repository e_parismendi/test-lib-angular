import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {

  @Input() account: any = {};

  constructor() { }

  ngOnInit(): void {
  }

  cuenta(event: any) {
    this.account = event.detail;
  }
} 
