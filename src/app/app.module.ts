import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { environment } from './../environments/environment';
import { config } from 'ba360-ui';
import { ContactComponent } from './contact/contact.component';
import { AccountComponent } from './account/account.component';

config(environment.companyName, environment.clientKey, environment.secretKey, environment.apiUrl);

@NgModule({
  declarations: [
    AppComponent,
    ContactComponent,
    AccountComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
